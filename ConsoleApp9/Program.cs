﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)

        {
            Console.WriteLine("Kolik hostů bude na párty?");
            int pocetHostu = Int32.Parse(Console.ReadLine());
            Random A = new Random();
            List<string> seznamHostu = new List<string>();
            while (pocetHostu > 0)
            {
                seznamHostu.Add(JmenoPrijmeni(A));
                pocetHostu--;
            }

            seznamHostu = seznamHostu.OrderBy(q => q).ToList();
            VypisList(seznamHostu);
        }

        public static string DejSamohlasku(Random random)
        {
            string[] poleZnaku = new string[5];
            poleZnaku[0] = "a";
            poleZnaku[1] = "e";
            poleZnaku[2] = "i";
            poleZnaku[3] = "o";
            poleZnaku[4] = "u";
            int nahodne = random.Next(0, poleZnaku.Length);
            string pismeno = poleZnaku[nahodne];

            return pismeno;
        }

        public static string DejSouhlasku(Random random)
        {
            string[] poleSouhlasek = new string[21];
            poleSouhlasek[0] = "b";
            poleSouhlasek[1] = "c";
            poleSouhlasek[2] = "d";
            poleSouhlasek[3] = "ch";
            poleSouhlasek[4] = "f";
            poleSouhlasek[5] = "g";
            poleSouhlasek[6] = "h";
            poleSouhlasek[7] = "j";
            poleSouhlasek[8] = "k";
            poleSouhlasek[9] = "l";
            poleSouhlasek[10] = "m";
            poleSouhlasek[11] = "n";
            poleSouhlasek[12] = "p";
            poleSouhlasek[13] = "q";
            poleSouhlasek[14] = "r";
            poleSouhlasek[15] = "s";
            poleSouhlasek[16] = "t";
            poleSouhlasek[17] = "v";
            poleSouhlasek[18] = "w";
            poleSouhlasek[19] = "x";
            poleSouhlasek[20] = "z";
            int nahodne = random.Next(0, poleSouhlasek.Length);
            string souhlaska = poleSouhlasek[nahodne];

            return souhlaska;
        }

        public static string DejSouhlaskuSamohlasku(Random random)
        {
            //Console.WriteLine(DejSouhlasku() + DejSamohlasku());

            return DejSouhlasku(random) + DejSamohlasku(random);
        }

        public static string DejSlabikuOVelikostiTri(Random random)
        {
            Random B = new Random();
            return DejSamohlasku(B) + DejSouhlaskuSamohlasku(B);
        }

        public static void DejPocetPismenVSlabice(int delka)
        {
            Random B = new Random();
            int i = 1;
            while (i <= delka)
            {
                if (i % 2 != 0)
                {
                    Console.Write(DejSamohlasku(B));
                }
                else
                {
                    Console.Write(DejSouhlasku(B));
                }
                i++;
            }

        }

        public static string DejJmeno(Random A)
        {
            int pocet = A.Next(1, 4);
            //Console.WriteLine(pocet);
            string jmeno = "";
            int a = 1;
            while (a <= pocet - 1)
            {
                //jmeno += DejSouhlaskuSamohlasku();
                jmeno = jmeno + DejSouhlaskuSamohlasku(A);
                a++;
            }
            DejSlabikuOVelikostiTri(A);
            jmeno += DejSouhlaskuSamohlasku(A);

            return PrvniPismenoVelke(jmeno);
        }

        public static string PrvniPismenoVelke(string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        public static string JmenoPrijmeni(Random A)
        {
            return DejJmeno(A) + " " + DejJmeno(A);
        }

        public static void VypisList(List<string> seznam)
        {
            foreach (var jmenoHosta in seznam)
            {
                Console.WriteLine(jmenoHosta);
            }
        }


        //Bonus 16
        public static int VypisNKralikuRekurze(int pocetkraliku)
        {
            if (pocetkraliku == 0)
            {
                return 1;
            }
            string nKraliku = pocetkraliku + ". kralik";
            int pocet = pocetkraliku * VypisNKralikuRekurze(pocetkraliku - 1);
            Console.WriteLine(nKraliku);
            return pocet;

        }

    }
}






